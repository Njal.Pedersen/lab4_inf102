package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int lo = 0;
        int hi = list.size() - 1;
        int middle = lo + (hi - lo) / 2;
        T ans = findmedian(listCopy, lo, hi, middle);
        return ans;
    }

    private <T extends Comparable<T>> T findmedian(List<T> list, int lo, int hi, int ind) {

        int j = dividconq(list, lo, hi);

        if (j == ind) {
            return list.get(j);
        } else if (j < ind) {
            return findmedian(list, j+1, hi, ind);
        } else {
            return findmedian(list, lo, j-1, ind);
        }
    }

    private <T extends Comparable<T>> int dividconq(List<T> list, int lo, int hi) {

        int i = lo, j = hi + 1;
        T v = list.get(lo);
        while (true) {
            while (less(list.get(++i), v)) {
                if (i >= hi) {
                    break;
                }
            }
            while (less(v, list.get(--j))) {
                if (j <= lo) {
                    break;
                }
            }
            if (i >= j) {
                break;
            }
            swap(list, i, j);
        }
        swap(list, lo, j);
        return j;
    }

    private <T extends Comparable<T>> void swap(List<T> list, int index1, int index2) {
        T temp = list.get(index1);
        list.set(index1, list.get(index2));
        list.set(index2, temp);
    }

    private <T extends Comparable<T>> boolean less(T elem1, T elem2) {
        return elem1.compareTo(elem2) < 0;
    }
}
