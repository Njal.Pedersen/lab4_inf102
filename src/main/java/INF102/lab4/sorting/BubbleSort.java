package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int size = list.size();
        int midlastswap = size;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                if (less(list.get(j+1), list.get(j))) {
                    swap(list, j, j+1);
                    midlastswap = j+1;
                }
            }
           
            if (isSorted(list)) {
                break;
            }
           
            
        }
        System.out.println("issorted: "+  isSorted(list));
    }

    private <T extends Comparable<T>> void swap(List<T> list, int index1, int index2) {
        T temp = list.get(index1);
        list.set(index1, list.get(index2));
        list.set(index2, temp);
    }

    private <T extends Comparable<T>> boolean less(T elem1, T elem2) {
        return elem1.compareTo(elem2) < 0;
    }

    private <T extends Comparable<T>> boolean isSorted(List<T> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            if (less(list.get(i+1), list.get(i))) {
                return false;
            }
        }
        return true;
    }


    
}
