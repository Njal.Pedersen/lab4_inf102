package INF102.lab4.sorting;

import java.util.Collections;
import java.util.List;

/**
 * Timsort is the sorting algorithm used by the Collections framework. It is a combination of merge sort and insertion sort.
 */
public class TimSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        Collections.sort(list); // This method performs timsort
        System.out.println("issorted timsort: "+ isSorted(list));           
    }

    private <T extends Comparable<T>> boolean isSorted(List<T> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            if (less(list.get(i+1), list.get(i))) {
                return false;
            }
        }
        return true;
    }

    private <T extends Comparable<T>> boolean less(T elem1, T elem2) {
        if (elem1.compareTo(elem2) < 0) {
            return true;
        }
        return false;
    }
    
}
